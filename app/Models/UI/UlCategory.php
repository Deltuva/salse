<?php

namespace App\Models\UI;
use App\Category;

class UlCategory
{
    public static function getCategories()
    {
        $categoriesCollection = Category::orderBy('position', 'asc')
            ->get();
        
        return $categoriesCollection;
    }
}