<?php

namespace App\Models\UI;
use Illuminate\Http\Request;

class ActivePage
{
    public static function getActiveSegment($slug)
    {
        if (\Request::segment(2) == $slug) {
            return true;
        } else {
            return false;
        }
    }

    public static function getActiveClass() {
        return 'class=active';
    }
}