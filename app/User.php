<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'permission'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getPermission() {
        return $this->attributes['permission'];
    }

    public function getName() {
        return $this->attributes['name'];
    }

    public function getAvatar() {
        return 'https://api.adorable.io/avatars/167/' . $this->attributes['email'] . '.png';
    }
}
