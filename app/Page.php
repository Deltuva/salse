<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use \Rutorika\Sortable\SortableTrait;

    protected $fillable = ['name', 'content', 'slug', 'position'];
}
