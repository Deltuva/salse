<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use \Rutorika\Sortable\SortableTrait;
    protected $fillable = ['name', 'icon'];

    public function getProducts()
    {
        return $this->hasMany('App\Product');
    }
}
