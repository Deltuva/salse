<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    public function productShow($productId = false) {
        $product = Product::whereId($productId)
            ->firstOrFail();

        return view('pages.product', compact('product'));
    }
}
