<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;

class ApiController extends Controller
{
    public function getProduct(Product $productModel, $productId)
    {
        try {
            $response = [];
            $productItem = $productModel->select('name', 'description')->whereId($productId)->first();
                
            if (!is_null($productItem)) {
                $response = [
                    'name' => $productItem->name,
                    'description' => $productItem->description
                ];
            }
        } catch (Exception $e) {
            //
        } finally {
            return response()
               ->json(['data' => $response]);
        }
    }
}
