<?php

namespace App\Http\Controllers\Admin\Pages;

use App\Http\Requests\Pages\PageCreateRequest;
use App\Http\Requests\Pages\PageEditRequest;
use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::sorted()->get();
        return view('admin.pages.index')->with(array(
            'pages' => $pages
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PageCreateRequest $request)
    {
        Page::create([
            'name' => $request->name,
            'content' => $request->content,
            'slug' => str_slug($request->name, 'slug')
        ]);

        session()->flash('success', 'Puslapis sėkmingai sukurtas.');
        return redirect()->route('pages.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        $page = Page::find($page->id);
        if($page) {
            return view('admin.pages.edit')->with(array(
                'page' => $page
            ));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(PageEditRequest $request, Page $page)
    {
        $page = Page::find($page->id);
        if($page) {

            $page->name = $request->name;
            $page->content = $request->content;
            $page->slug = str_slug($request->name, '-');
            $page->save();

            session()->flash('success', 'Puslapis sėkmingai pridėtas');
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        $page = Page::find($page->id);
        if($page) {
            $page->delete();
            session()->flash('success', 'Puslapis sėkmingai ištrintas.');
        }

        return redirect()->back();
    }
}
