<?php

namespace App\Http\Controllers\Admin\Products;

use App\Category;
use App\Http\Requests\Product\ProductCreateRequest;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::with('category')->paginate(10);
        return view('admin.products.index')->with(array(
            'products' => $products,
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::get();
        return view('admin.products.create')->with(array(
            'categories' => $categories
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductCreateRequest $request)
    {
        Product::create([
            'name' => $request->name,
            'description' => $request->description,
            'category_id' => $request->category
        ]);

        session()->flash('success', 'Produktas sėkmingai sukurtas.');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $product = Product::find($product->id);
        if($product) {
            $categories = Category::get();
            return view('admin.products.edit')->with(array(
                'product' => $product,
                'categories' => $categories,
            ));
        } else {
            redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $product = Product::find($product->id);
        if($product) {
            $product->name = $request->name;
            $product->description = $request->description;
            $product->category_id = $request->category;
            $product->save();

            session()->flash('success', 'Produktas pakoreguotas.');
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product = Product::find($product->id);
        if($product) {
            $product->delete();
        }

        return redirect()->back();
    }
}
