<?php

namespace App\Http\Controllers\Admin\Posts;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Posts\PostCreateRequest;
use App\Http\Requests\Posts\PostEditRequest;
use App\Post;
use Image;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderby('id', 'desc')->paginate(20);
        return view('admin.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostCreateRequest $request)
    {   
        if($request->hasFile('photo')) {

            $destinationPath = base_path() . '/public/uploads/posts/'; // upload path
            
            if($request->file('photo')) {
                $file = $request->file('photo');
                $file_name_ext = $file->getClientOriginalExtension();
                $generated_id = uniqid();

                $ul = $generated_id . '.' . strtolower($file_name_ext);

                // uploading 

                $img = Image::make($file);
                $img->save($destinationPath . $ul);

                if(file_exists($destinationPath . $ul)) {

                    Post::create([
                        'title' => $request->title,
                        'body' => $request->body,
                        'slug' => str_slug($request->slug),
                        'photo' => $ul,
                    ]);

                    session()->flash('success', 'Naujiena sėkmingai sukurta.');
                } else {
                    session()->flash('error', 'Naujiena nepatalpinta.');
                }
            }

        } else {
            session()->flash('error', 'Naujiena nepatalpinta.');
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        if($post) {
            return view('admin.posts.edit', compact('post'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostEditRequest $request, $id)
    {
        $post = Post::find($id);
        if($post) {
            $post->title = $request->title;
            $post->body = $request->body;
            $post->slug = str_slug($request->title);

            if($request->hasFile('photo')) {

                $destinationPath = base_path() . '/public/uploads/posts/'; // upload path
                
                $file = $request->file('photo');
                
                $file_name_ext = $file->getClientOriginalExtension();
                $generated_id = uniqid();

                $ul = $generated_id . '.' . strtolower($file_name_ext);

                $img = Image::make($file);
                $img->save($destinationPath . $ul);

                if(file_exists($destinationPath . $ul)) {
                    $post->photo = $ul;

                    session()->flash('success', 'Naujiena sėkmingai atnaujinta.');
                } else {
                    session()->flash('error', 'Naujienos nuotrauka nepatalpinta.');
                }

            }


            $post->save();

            session()->flash('success', 'Naujiena sėkmingai atnaujinta..');
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        if($post) {
            $post->delete();
        }

        return redirect()->back();
    }
}
