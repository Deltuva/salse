<?php

namespace App\Http\Controllers\Admin\Settings;

use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Image;
use Cache;

class SettingsController extends Controller
{   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings_ = Setting::orderBy('id', 'desc');

        if(isset(request()->search)) {
            $settings_->where('name', 'like', '%' . request()->search . '%');
        }

        $settings = $settings_->paginate(20);
        return view('admin.settings.index')->with(array(
            'settings' => $settings
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show(Setting $setting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        $setting = Setting::find($setting->id);
        if($setting) {
            return view('admin.settings.edit')->with(array(
                'settings' => $setting
            ));
        } else {
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Setting $setting)
    {   
        $setting = Setting::find($setting->id);
        if($setting) {
            if($setting->type == 'string') {
                $validator = Validator::make($request->all(), [
                    'value' => 'required'
                ], [
                    'value.required' => 'šis laukelis privalomas'
                ]);

                $setting->value = $request->value;
            }

            if($setting->type == 'integer') {
                $validator = Validator::make($request->all(), [
                    'value' => 'required|numeric'
                ], [
                    'value.required' => 'šis laukelis privalomas',
                    'value.numeric' => 'Čia galimą įrašyti tik skaičius.'
                ]);

                $setting->value = $request->value;
            }

            if($setting->type == 'image') {
                $validator = Validator::make($request->all(), [
                    'value' => 'required|image'
                ], [
                    'value.image' => 'Nuotraikos palaikomi formatai: png, jpg, jpeg, bmp'
                ]);

                if($validator->passes()) {
                    if($request->hasFile('value')) {
                        $destinationPath = base_path() . '/public/uploads/settings/'; // upload path
                    
                        $file = $request->file('value');
                        
                        $file_name_ext = $file->getClientOriginalExtension();
                        $generated_id = uniqid();

                        $ul = $generated_id . '.' . strtolower($file_name_ext);

                        $img = Image::make($file);
                        $img->save($destinationPath . $ul);
                        $setting->value = $ul;
                    }
                }
            }

            if($setting->type == 'editor') {
                $validator = Validator::make($request->all(), [
                    'value' => 'required'
                ], [
                    'value.required' => 'šis laukelis privalomas',
                ]);

                $setting->value = $request->value;
            }

            if($setting->type == 'select') {
                $validator = Validator::make($request->all(), [
                    'value' => 'required|not_in:""'
                ], [
                    'value.required' => 'šis laukelis privalomas',
                ]);

                $setting->value = $request->value;
            }

            if($setting->type == 'youtube') {
                $validator = Validator::make($request->all(), [
                    'value' => 'required'
                ], [
                    'value.required' => 'šis laukelis privalomas',
                ]);

                $setting->value = $request->value;
            }

            // if all good save and redirect back
            if($validator->passes()) {
                $setting->save();

                // forgot cache by key
                if(Cache::has($setting->key)) {
                    Cache::forget($setting->key);
                }

                // caching setting value
                Cache::forever($setting->key, $setting->value);

                session()->flash('success', 'Nustatymas sekmingai atnaujintas');

                return redirect()->back();
            }

            // if fail redirect back with errors..
            if($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            }         

        } else {
            session()->flash('error', 'Klaida, tokio nustatymo nera.');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
        //
    }
}
