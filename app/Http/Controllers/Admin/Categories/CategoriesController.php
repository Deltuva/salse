<?php

namespace App\Http\Controllers\Admin\Categories;

use App\Category;
use App\Http\Requests\Categories\CategoriesCreateRequest;
use App\Http\Requests\Categories\CategoriesEditRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::sorted()->get();
        return view('admin.categories.index')->with(array(
            'categories' => $categories
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriesCreateRequest $request)
    {
        if($request->hasFile('icon')) {

            $destinationPath = base_path() . '/public/uploads/categories/icons/'; // upload path

            if($request->file('icon')) {
                $file = $request->file('icon');
                $file_name_ext = $file->getClientOriginalExtension();
                $generated_id = uniqid();

                $ul = $generated_id . '.' . strtolower($file_name_ext);

                // uploading

                $img = Image::make($file);
                $img->save($destinationPath . $ul);

                if(file_exists($destinationPath . $ul)) {
                    Category::create([
                        'name' => $request->name,
                        'icon' => $ul,
                    ]);

                    session()->flash('success', 'Kategorija sėkmingai sukurta.');
                } else {
                    session()->flash('error', 'Kategorija nepatalpinta.');
                }
            }
        } else {
            session()->flash('error', 'Naujiena nepatalpinta.');
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $category = Category::find($category->id);
        if($category) {
            return view('admin.categories.edit')->with(array(
                'category' => $category
            ));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoriesEditRequest $request, Category $category)
    {
        $category = Category::find($category->id);
        if($category) {
            $category->name = $request->name;

            if($request->hasFile('icon')) {

                $destinationPath = base_path() . '/public/uploads/categories/icons/'; // upload path

                if($request->file('icon')) {
                    $file = $request->file('icon');
                    $file_name_ext = $file->getClientOriginalExtension();
                    $generated_id = uniqid();

                    $ul = $generated_id . '.' . strtolower($file_name_ext);

                    // uploading

                    $img = Image::make($file);
                    $img->save($destinationPath . $ul);

                    if(file_exists($destinationPath . $ul)) {

                        $category->icon = $ul;

                        session()->flash('success', 'Kategorija sėkmingai sukurta.');
                    } else {
                        session()->flash('error', 'Kategorija nepatalpinta.');
                    }
                }
            } else {
                session()->flash('error', 'Naujiena nepatalpinta.');
            }

            $category->save();

            session()->flash('success', 'Kategorija sėkmingai atnaujinta.');
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category = Category::find($category->id);
        if($category) {
            $category->delete();

            session()->flash('success', 'Kategorija sekmingai ištrinta.');
        }

        return redirect()->back();
    }
}
