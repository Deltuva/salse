<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
    const POSTS = 20;

    public function index() {
        $posts = Post::orderBy('id', 'desc')
            ->paginate(self::POSTS);

        return view('news', compact('posts'));
    }

    public function postShow($slug) {
        $post = Post::whereSlug($slug)
            ->firstOrFail();

        return view('news.view', compact('post'));
    }
}
