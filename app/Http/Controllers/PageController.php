<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Post;
use App\Product;

class PageController extends Controller
{
    const POSTS = 20;
    protected $templateDir = 'pages/';

    /**
     * Template page
     */
    public function pageShow($slug = false) {
        $page = Page::whereSlug($slug)
            ->firstOrFail();
        $template = $page->content_class;
        
        if (!is_null($template)) {
            $response = $this->pageResponse($template, $page);
        }

        return view($this->templateDir. '.'.$template, $response);
    }

    /**
     * Response template data
     */
    public function pageResponse($pageClass, $page, $productId = 1) {
        if ($pageClass == 'about') {

            $this->data['page'] = $page;

        } else if ($pageClass == 'product') {
            
            $this->data['page'] = $page;
            $this->data['product'] = Product::whereId($productId)
                ->firstOrFail();

        } else if ($pageClass == 'new') {

            $this->data['page'] = $page;
            $this->data['posts'] = Post::orderBy('id', 'desc')
                ->paginate(self::POSTS);

        } else if ($pageClass == 'contact') {
            
            $this->data['page'] = $page; 
        }

        return $this->data;
    }
}
