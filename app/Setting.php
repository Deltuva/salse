<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
	protected $fillable = ['name', 'value', 'type', 'key', 'description'];

	public function getType() {
		return $this->attributes['types'];
	}
}
