<?php 
	namespace App\Helpers;

	use App\Setting;
	use Cache;

	class Helpers {
		public static function config($key) {
			if(Cache::has($key)) {
				return Cache::get($key, 'Setting with name ' . $key . ' is corrupted.');
			} else {
				$settings = Setting::where('key', $key)->first();
				if($settings) {
					if($settings->value == NULL) {
						return $key . ' value is not defined..';
					} else {
						Cache::forever($settings->key, $settings->value);
						return $settings->value;
					}
				} else {
					return $key;
				}
			}
		}
	}