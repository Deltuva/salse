<?php

return [
    'entities' => [
        'pages' => '\App\Page',
        'categories' => '\App\Category',
        // 'articles' => '\Article' for simple sorting (entityName => entityModel) or
        // 'posts' => ['entity' => '\Post', 'relation' => 'tags'] for many to many or many to many polymorphic relation sorting
    ],
];
