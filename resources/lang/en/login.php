<?php
    return [
        'title' => 'Login',
        'email' => 'E-mail Address',
        'password' => 'Password',
        'rememberMe' => 'Remember me',
        'login' => 'Login',
        'forgotPassword' => 'Forgot Your password?',
    ]
?>