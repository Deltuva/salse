<?php 
    return [
        'title' => 'Reset Password',
        'email' => 'Email',
        'sendForgotPassword' => 'Send Password Reset link',
    ]
?>