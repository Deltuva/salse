<?php
    return [
        'title' => 'Register',
        'name' => 'Name',
        'email' => 'E-Mail address',
        'password' => 'Password',
        'confirm_password' => 'Confirm Password',
        'register' => 'Register',
    ]
?>