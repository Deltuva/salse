$(document).ready(function () {

    let products = 'ul.ul-products li';
    let countProducts = $('ul.ul-products li').length;
    let productsShowNum = 3;

    $("ul.ul-categories > li").click(function () {
        $(this).addClass('active').siblings().removeClass('active');

        // hide next childs
        // if ($(this).hasClass('active')) {
        //     $(this).toggleClass("open");
        // }
        // if ($(this).hasClass('open')) {
        //     $(this).toggleClass("show");
        //     $("ul.ul-categories > li").addClass("active");
        //     $("ul.ul-categories > li").toggleClass("hide");
        // }

        $(this).next("ul").slideToggle();
    });

    $("ul.ul-products > li").click(function () {
        let productId = $(this).attr('data-product-id');

        $(this).addClass('active').siblings().removeClass('active');
        getProduct(productId);
    });

    $('.ul-products li:lt(' + productsShowNum + ')').show();
    $('.loadMore').click(function () {
        let n = (productsShowNum + 5 <= countProducts) ? productsShowNum + 5 : countProducts;
        $('.ul-products li:lt(' + n + ')').show();
    });

    $('.showLess').click(function () {
        let n = (productsShowNum - 5 < 0) ? 3 : productsShowNum - 5;
        $('.ul-products li').not(':lt(' + n + ')').hide();
    });

    function getProduct(id) {
        let _API_URL = "http://localhost:8000/ajax/product";
        axios.get(_API_URL + '/' + id)
            .then((response) => {
                let name = response.data.data.name;
                let description = response.data.data.description;

                $(".siteHeadingTitle").html(name);
                $(".siteParagraphText").html(description);
            }, (response) => {
                // error callback
            });

        //console.log("Product.." + id);
    }
});