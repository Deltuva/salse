<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
    <div class="siteContent__block">
        @if (!is_null($product->name))
            <div class="siteHeadingTitle">
                {{ $product->name }}
            </div>
        @endif
        
        @if (!is_null($product->name))
            <div class="siteParagraphText">
                {{ $product->description }}
            </div>
        @endif
    </div>
</div>