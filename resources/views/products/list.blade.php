<section class="siteContent">
    <div class="container">
        <div class="row no-margin">
            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                <div class="siteContent__categories">
                    @include('includes/categories')
                </div>
            </div>

            @include('products/view')
        </div>
    </div>
</section>