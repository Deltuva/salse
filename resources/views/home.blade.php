@extends('layouts.app') @section('content')

<div class="siteBg top" style="background-image: url(/images/backgrounds/home.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @include('includes/header')
            </div>
        </div>
    </div>

    <section class="siteContent">
        <div class="container">
            <div class="row no-margin">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="siteContent__categories">
                        @include('includes/categories')
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                    <div class="siteContent__block">
                        <div class="siteHeadingTitle">
                            emporibus autem quibusdam et
                        </div>
                        <div class="siteParagraphText">
                            Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint
                            et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis
                            voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repella
                        </div>
                        <div class="siteRightAuthor text-right">
                            UAB, Salsė
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection