<section class="siteContent {{ $page->content_class }}">
    <div class="container">
        <div class="row no-margin">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h3 class="text-center">Mus rasite:</h3>

                <div class="siteContent__map">
                    <div id="map"></div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h3>Rekvizitai</h3>

                {!! $page->content !!}
            </div>
        </div>
    </div>
</section>

@push('scripts')
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_JS_API') }}&callback=initMap"></script>
<script type="text/javascript">
	function initMap() {
			var marker;
			var mapOptions = {
				zoom: 16,
				center: new google.maps.LatLng(56.0600622, 21.4835102),
				mapTypeId: google.maps.MapTypeId.TERRAIN,
			};

			map = new google.maps.Map(document.getElementById('map'), mapOptions);

			var marker = new google.maps.Marker({
				position: {
					lat: 56.0600622,
					lng: 21.4835102,
				},
				map: map
			});
		}
</script>
@endpush