<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="{{ asset('images/favicon.png') }}">
    {{--  <meta http-equiv="Content-Security-Policy" content="default-src {{ env('CONTENT_SECURITY_POLICY_DOMAINS') }}; child-src 'none'; object-src 'none'">  --}}

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Salsė') }}</title>
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}?t={{ time() }}" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <![endif]-->

    <meta name="application-name" content="Sėklos su pagarba Jums ir Jūsų verslui"/>
    <meta name="msapplication-TileColor" content="#30ce14"/>

</head>
<body>
   
    <div id="app">
        @yield('content')
        @include('includes/footer')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/all.js') }}?t={{ time() }}"></script>

    @stack('scripts')
</body>
</html>