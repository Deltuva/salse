<div class="header">
    <div class="header__topline">
        <span class="social">
            <a href="#">
                <i class="fa fa-facebook" aria-hidden="true"></i>
                Facebook</a>
            <a href="#">
                <i class="fa fa-youtube-play" aria-hidden="true"></i>
                Youtube</a>
        </span>
    </div>
    @include('includes/navbar')
</div>