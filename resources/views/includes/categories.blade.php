<nav class="categories">
    <?php $categories = \App\Models\UI\UlCategory::getCategories(); ?>

    @if (count($categories) > 0)
        <ul class="ul-categories">
            @foreach ($categories as $category)
                <li>
                    @if (!is_null($category->name))
                        <a href="#"><i class="{{ $category->icon }}"></i> {{ $category->name }}</a>
                    @endif
                </li>
                @if($category->getProducts()->count() > 0)
                    <ul class="ul-products">
                        @foreach ($category->getProducts()->get() as $product)
                            @if (!is_null($product->name))
                                <li data-product-id="{{ $product->id }}"><a>{{ $product->name }}</a></li>
                            @endif
                        @endforeach

                        {{--  <div class="loadMore">Daugiau produktų</div>
                        <div class="showLess">Mažiau</div>  --}}
                    </ul>
                @endif
            @endforeach
        </ul>
        @else
            <span class="empty">
                Kategorijų nėra.
            </span>
    @endif
</nav>