<!-- Nav -->
<nav class="navbar navbar-default">
    <div class="container">
        <!--Navbar Header Start Here-->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mobile">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">
                <!-- Logos -->
                <img class="visible-xs visible-sm visible-md" src="/images/logo-mobile.png" alt="Salsė">
                <img class="hidden-xs hidden-sm hidden-md" src="/images/logo.png" alt="Salsė">
                <!-- Logos end -->
            </a>
        </div>
        <!--Navbar Header End Here-->
        <!--Menu Start Here-->
        <div class="collapse navbar-collapse" id="mobile">

            <?php $menu_pages = \App\Models\UI\Menu::getPages(); ?>

            @if (count($menu_pages) > 0)
            <ul class="nav navbar-nav navbar-right">
                @foreach ($menu_pages as $menu_page)
                    @if (\App\Models\UI\ActivePage::getActiveSegment($menu_page->slug))

                        <?php $menu_page_active = \App\Models\UI\ActivePage::getActiveClass(); ?>
                        <li {{ $menu_page_active }}>
                    @else   
                        <li>
                    @endif
                        @if (!is_null($menu_page->name))
                            <a href="{!! route('pageShow',[$menu_page->slug]); !!}">{{ $menu_page->name }}</a>
                        @endif
                    </li>
                @endforeach
            </ul>
            @endif
        </div>
        <!--Menu End Here-->
    </div>
</nav>
<!-- Nav end-->