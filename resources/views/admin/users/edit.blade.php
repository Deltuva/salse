@extends('admin.layouts.master')
@section('content')
    @include('admin.includes.header',[
        'title' => 'Naudotojo redagavimas'
    ])

    <div class="content">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Readguoti {{ $user->email }} naudotoją <a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                @include('admin.includes.alert')

                <form action="{{ route('users.update', $user->id) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label>Name</label>
                        <input type="text" class="form-control" value="{{ $user->name }}" placeholder="Slapyvardis" name="name">
                        <p class="help-block">{{ $errors->first('name', ':message') }}</p>
                    </div>

                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        <label>El. paštas</label>
                        <input type="email" class="form-control" value="{{ $user->email }}" placeholder="El. paštas" name="email">
                        <p class="help-block">{{ $errors->first('name', ':message') }}</p>
                    </div>

                    <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                        <label>Slaptažodis</label>
                        <input type="password" class="form-control" value="{{ old('password') }}" placeholder="Slaptažodis" name="password">
                        <p class="help-block">{{ $errors->first('name', ':message') }}</p>
                    </div>

                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label>Pakartokite slaptažodis</label>
                        <input type="password" class="form-control" value="{{ old('repeat_password') }}" placeholder="Pakartokite slaptažodis" name="repeat_password">
                        <p class="help-block">{{ $errors->first('name', ':message') }}</p>
                    </div>

                    <button class="btn btn-primary">Atnaujinti</button>
                </form>
            </div>
        </div>
        <!-- /simple panel -->

    </div>
    <!-- /content area -->

@endsection