@extends('admin.layouts.master')
@section('content')
    @include('admin.includes.header',[
        'title' => 'Naudotojai'
    ])

    <div class="content">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Naudotojai <a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                @include('admin.includes.alert')

                <form action="{{ route('users.store') }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}

                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label>Slapyvardis</label>
                        <input type="text" class="form-control" value="{{ old('name') }}" placeholder="Slapyvardis" name="name">
                        <p class="help-block">{{ $errors->first('name', ':message') }}</p>
                    </div>

                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        <label>El. paštas</label>
                        <input type="email" class="form-control" value="{{ old('email') }}" placeholder="El. paštas" name="email">
                        <p class="help-block">{{ $errors->first('name', ':message') }}</p>
                    </div>

                    <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                        <label>Slaptažodis</label>
                        <input type="password" class="form-control" value="{{ old('password') }}" placeholder="Slaptažodis" name="password">
                        <p class="help-block">{{ $errors->first('name', ':message') }}</p>
                    </div>

                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label>Pakartokite slaptažodį</label>
                        <input type="password" class="form-control" value="{{ old('repeat_password') }}" placeholder="Pakartokite slaptažodį" name="repeat_password">
                        <p class="help-block">{{ $errors->first('name', ':message') }}</p>
                    </div>s

                    <button class="btn btn-primary">Pridėti</button>
                </form>
            </div>

            @if(!$users->isEmpty())
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Slapyvardis</td>
                                <td>El. pašto adreasas</td>
                                <td>Sukurtas</td>
                                <td>Atnaujintas</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->created_at }}</td>
                                    <td>{{ $user->updated_at }}</td>
                                    <td><a href="{{ route('users.edit', $user->id) }}" class="btn btn-primary">Redaguoti</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="panel-body">
                    <div class="text-center">
                        {{ $users->links() }}
                    </div>
                </div>
            @else
                <div class="panel-body">
                    <div class="alert-danger alert">Naudotoju nėra.</div>
                </div>
            @endif
        </div>
        <!-- /simple panel -->

    </div>
    <!-- /content area -->

@endsection