@extends('admin.layouts.master')
@section('content')
    @include('admin.includes.header',[
        'title' => 'Nustatymai',
        'description' => 'Visi svetainės nustatymai',
        'breadcrumps' => [
            [
                'title' => 'Nustatymai',
                'route' => route('settings.index')
            ]
        ]
    ])
    <!-- Content area -->
    <div class="content">
        <!-- Simple panel -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Nustatymai<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <form method="get">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Raktažodis</label>
                                <input type="text" name="search" value="{{ old('search') }}" class="form-control" placeholder="Įveskite raktažodį arba sakinį susijusi su nustatymų.">
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary">Ieškoti</button>
                </form>
            </div>

            @if(!$settings->isEmpty())
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <td>#</td>
                            <td>Pavadinimas</td>
                            <td>Aprašymas</td>
                            <td>Reikšmė</td>
                            <td>-</td>
                        </tr>
                        <tbody>
                            @foreach($settings as $setting)
                                <tr class="small">
                                    <td>{{ $setting->id }}</td>
                                    <td>{{ $setting->name }}</td>
                                    <td>{{ $setting->description }}</td>
                                    <td>
                                        @if(!empty($setting->value))
                                            @if($setting->type == 'image')
                                                @if(file_exists('uploads/settings/' . $setting->value))
                                                    <img src="{{ asset('uploads/settings/' . $setting->value) }}" class="img-rounded" width="100" alt="Setting image">
                                                @else
                                                    Paveiksliuko nepavyko rasti.
                                                @endif
                                            @else
                                                {{ $setting->value }}
                                            @endif
                                        @else
                                            Nenustatyta
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('settings.edit', $setting->id) }}" class="btn btn-primary">Redaguoti</a>
                                    </td>
                                </tr>   
                            @endforeach 
                        </tbody>
                        
                    </thead>
                </table>
        
                <div class="panel-body">
                    <div class="text-center">
                        {{ $settings->links() }}
                    </div>
                </div>
            @else
                <div class="panel-body">
                    <div class="alert alert-danger">
                        Klaida, nustatymu nera.
                    </div>
                </div>
            @endif
        </div>

        <!-- /simple panel -->

    </div>
    <!-- /content area -->

@endsection