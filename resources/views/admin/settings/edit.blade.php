@extends('admin.layouts.master')
@section('content')
    @include('admin.includes.header',[
        'title' => 'Redauguoti nustatymą',
        'description' => 'Paspaudus migtuką "Išsaugoti" nustatymai isigalios iškarto.',
        'breadcrumps' => [
            [
                'title' => 'Nustatymai',
                'route' => route('settings.index')
            ],
            [
                'title' => 'Keisti nustatymą "' . $settings->name . '"',
                'route' => route('settings.edit', $settings->id)
            ],
        ]
    ])
    <!-- Content area -->
    <div class="content">
        <!-- Simple panel -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Redaugoti nustatymą<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                @include('admin.includes.alert')

                <form action="{{ route('settings.update', $settings->id) }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    
                    <p class="alert alert-info">{{ $settings->description }}</p>

                    @if($settings->type == 'image')
                        @if(file_exists('uploads/settings/' . $settings->value))
                            <div class="text-center">
                                <div>Darbartinė nuotrauką</div>
                                <img src="{{ asset('uploads/settings/' . $settings->value) }}" class="img-rounded  " width="300" alt="Setting image">
                            </div>
                            <br>
                        @endif
                    @endif
                    <div class="form-group {{ $errors->has('value') ? 'has-error' : '' }}">
                        <label>{{ $settings->name }}</label>

                        @if($settings->type == 'string')
                            <input type="text" name="value" class="form-control" autocomplete="off" value="{{ $settings->value }}" placeholder="{{ $settings->name }}">
                        @endif

                        @if($settings->type == 'integer')
                            <input type="number" name="value" class="form-control" autocomplete="off" value="{{ $settings->value }}" placeholder="{{ $settings->name }}">
                        @endif

                        @if($settings->type == 'youtube')
                            <input type="text" name="value" class="form-control" autocomplete="off" value="{{ $settings->value }}" placeholder="{{ $settings->name }}">
                        @endif

                        @if($settings->type == 'select')
                            <select name="value" class="form-control">
                                <option value="">Pasirinkite</option>
                                <option @if($settings->value == 'Taip') selected="" @endif value="Taip">Taip</option>
                                <option @if($settings->value == 'Ne') selected="" @endif value="Ne">Ne</option>
                            </select>
                        @endif

                        @if($settings->type == 'image')
                            <input type="file" name="value" class="form-control">
                        @endif

                        <p class="help-block">{{ $errors->first('value', ':message') }}</p>
                    </div>

                    <button class="btn btn-primary">Keisti</button>
                    <a href="{{ route('settings.index') }}" class="btn btn-default">Grįžti atgal</a>
                </form>
            </div>
        </div>
        <!-- /simple panel -->

    </div>
    <!-- /content area -->

@endsection