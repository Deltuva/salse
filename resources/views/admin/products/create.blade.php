@extends('admin.layouts.master')
@section('content')
    @include('admin.includes.header',[
        'title' => 'Kurti produktą'
    ])
    <!-- Content area -->
    <div class="content">
        <!-- Simple panel -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Kurti produktą<a class="heading-elements-toggle"><i class="icon-box"></i></a></h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                @include('admin.includes.alert')
                <form method="post" action="{{ route('products.store') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}

                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        <label>Pavadinimas</label>
                        <input class="form-control" name="name" placeholder="Pavadinimas">
                        <p class="help-block">{{ $errors->has('name') ? $errors->first('name') : '' }}</p>
                    </div>

                    <div class="form-group {{ ($errors->has('description') ? 'has-error' : '') }}">
                        <label>Produkto aprašymas</label>
                        <textarea id="editor1" class="form-control input-sm" name="description" placeholder="Produkto aprašymas" rows="10"></textarea>
                        <p class="help-block">{{ $errors->first('description', ':message') }}</p>
                    </div>

                    <div class="form-group {{ ($errors->has('categories') ? 'has-error' : '') }}">
                        <label>Kategorija</label>
                        <select name="category" class="form-control">
                            <option value="">Pasirinkite kategorija</option>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                        <p class="help-block">{{ $errors->first('category', ':message') }}</p>
                    </div>

                    <button class="btn btn-primary btn-sm">Kurti</button>
                    <a href="{{ route('products.index') }}" class="btn btn-default btn-sm">Grįžti atgal</a>
                </form>
            </div>
        </div>
        <!-- /simple panel -->

    </div>
    <!-- /content area -->
@endsection