<div class="sidebar sidebar-main">
	<div class="sidebar-content">

		<!-- User menu -->
		<div class="sidebar-user">
			<div class="category-content">
				<div class="media">
					<a href="#" class="media-left"><img src="{{ asset(Auth::user()->getAvatar()) }}" class="img-circle img-sm" alt=""></a>
					<div class="media-body">
						<span class="media-heading text-semibold">{{ Auth::user()->getName() }}</span>
						<div class="text-size-mini text-muted">
							<i class="icon-user text-size-small"></i> {{ Auth::user()->getPermission() }}&nbsp;
						</div>
					</div>

					<div class="media-right media-middle">
						<ul class="icons-list">
							<li>
								<a href="#"><i class="icon-cog3"></i></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- /user menu -->


		<!-- Main navigation -->
		<div class="sidebar-category sidebar-category-visible">
			<div class="category-content no-padding">
				<ul class="navigation navigation-main navigation-accordion">
					<!-- Main -->
					<li class="navigation-header"><span>Pagrindiniai</span> <i class="icon-menu" title="" data-original-title="Pagrindiniai"></i></li>

                    <li><a href="{{ route('admin.home') }}"><i class="icon-home4"></i> <span>Skydelis</span></a></li>
                    <li><a href="{{ route('users.index') }}"><i class="icon-users"></i> <span>Naudotojai</span></a></li>
                    <li><a href="{{ route('pages.index') }}"><i class="icon-box"></i> <span>Puslapiai</span></a></li>
                    <li><a href="{{ route('categories.index') }}"><i class="icon-library2"></i> <span>Kategorijos</span></a></li>
                    <li><a href="{{ route('products.index') }}"><i class="icon-accessibility"></i> <span>Produktai</span></a></li>
                    <li><a href="{{ route('posts.index') }}"><i class="icon-list"></i> <span>Naujienos</span></a></li>
                    <li><a href="{{ route('settings.index') }}"><i class="icon-gear"></i> <span>Nustatymai</span></a></li>
				</ul>
			</div>
		</div>
		<!-- /main navigation -->

	</div>
</div>