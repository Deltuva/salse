
<div class="page-header page-header" style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">
    <div class="page-header-content">
        <div class="page-title">
            <h5>
                <i class="icon-arrow-left52 position-left"></i>
                @if(!empty($title))
                    <span class="text-semibold">{{ $title }}</span>
                @endif

                @if(!empty($description))
                    <small class="display-block">{{ $description }}</small>
                @endif
            </h5>
            <a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

        <div class="heading-elements">
            <button class="btn btn-link btn-icon btn-sm heading-btn"><i class="icon-gear"></i></button>
        </div>
    </div>

    <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="#">Current</a></li>
        </ul>

        <ul class="breadcrumb-elements">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-cog5 position-left"></i> Veiksmai
                    <span class="caret"></span>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#" onclick="window.print();"><i class="icon-printer pull-right"></i> Spausdinti</a></li>
                    @if(!empty($actions))
                        @foreach($actions as $action)
                            <li>
                                <a href="{{ (empty($action['route']) ? '#' : $action['route']) }}">
                                    <i class="{{ (empty($action['icon']) ? 'icon-more' : $action['icon']) }} pull-right"></i>
                                    {{ (empty($action['title']) ? 'Not defined' : $action['title']) }}
                                </a>
                            </li>
                        @endforeach
                    @endif
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /page header -->