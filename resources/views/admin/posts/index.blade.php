@extends('admin.layouts.master')
@section('content')
    @include('admin.includes.header',[
        'title' => 'Puslapiai',
        'description' => 'Svetainės naujienų koregavimas.',
        'actions' => [
            [
                'title' => 'Kurti naujieną',
                'route' => route('posts.create')
            ]
        ]
    ])

    <!-- Content area -->
    <div class="content">
        <!-- Simple panel -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Naujienos<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            @if(!$posts->isEmpty())
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Pavadinimas</td>
                                <td>Trumpas aprašymas</td>
                                <td>Nuotrauka</td>
                                <td>-</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($posts as $post)
                                <tr class="small">
                                    <td>{{ $post->id }}</td>
                                    <td>{{ $post->title }}</td>
                                    <td>{{ str_limit(strip_tags($post->body), 200) }}</td>
                                    <td>
                                        @if(file_exists('uploads/posts/' . $post->photo))
                                            <img class="img-rounded" width="100" src="{{ asset('uploads/posts/' . $post->photo) }}" alt="{{ $post->photo }}">
                                        @else
                                            Nepriskirta
                                        @endif
                                    </td>
                                    <td><a href="{{ route('posts.edit', $post->id) }}" class="btn btn-primary">Redaguoti</a></td>
                                    <td>
                                        <form action="{{ route('posts.destroy', $post->id) }}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button onclick="return confirm('Ar tikrai norite ištrinti puslapį?');" type="submit" class="btn btn-danger btn-sm">Pašalinti</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="panel-body">
                	<div class="text-center">
                		{{ $posts->links() }}
                	</div>
                </div>
            @else
                <div class="panel-body">
                    <div class="alert alert-danger">Naujienu nera.</div>
                </div>
            @endif
        </div>
        <!-- /simple panel -->

    </div>
    <!-- /content area -->

@endsection