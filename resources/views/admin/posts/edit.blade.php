@extends('admin.layouts.master')
@section('content')
    @include('admin.includes.header',[
        'title' => 'Naujienos redagavimas',
        'description' => 'Naujenos redagavimas'
    ])
    <div class="content">
        <div class="panel panel-flat">
            <div class="panel-body">

                @include('admin.includes.alert')

                <form action="{{ route('posts.update', $post->id) }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="form-group {{ ($errors->has('title') ? 'has-error' : '') }}">
                        <label>Pavadinimas</label>
                        <input class="form-control input-sm" placeholder="Pavadinimas" name="title" value="{{ $post->title }}">
                        <p class="help-block">{{ $errors->first('title', ':message') }}</p>
                    </div>

                    <div class="form-group {{ ($errors->has('body') ? 'has-error' : '') }}">
                        <label>Tekstas</label>
                        <textarea id="editor1" class="form-control input-sm" name="body" placeholder="Naujienos tekstas" rows="10">{{ $post->body }}
                        </textarea>
                        <p class="help-block">{{ $errors->first('body', ':message') }}</p>
                    </div>
                    <div class="text-center">
                        <img width="300" src="{{ asset('uploads/posts/' . $post->photo) }}" class="img-rounded">
                    </div>
                    <div class="form-group {{ $errors->has('photo') ? ' has-error' : '' }}">
                        <label>Paveiksliukas</label>
                        <input type="file" name="photo" class="form-control">
                        <p class="help-block">{{ $errors->has('photo') ? $errors->first('photo') : '' }}</p>
                    </div>

                    <button type="submit" class="btn btn-primary btn-sm">Redaugoti</button>
                    <a class="btn btn-default btn-sm" href="{{ route('posts.index') }}">Grįžti</a>
                </form>
            </div>
        </div>
    </div>
    @push('admin-scripts')
    <script src="//cdn.ckeditor.com/4.7.1/full/ckeditor.js"></script>
    <script>
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace( 'editor1' );
    </script>
    @endpush
@endsection