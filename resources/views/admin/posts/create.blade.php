@extends('admin.layouts.master')
@section('content')
    @include('admin.includes.header',[
        'title' => 'Kurti naujieną',
        'description' => 'Naujienos pridejimas',
    ])

    <!-- Content area -->
    <div class="content">
        <!-- Simple panel -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Naujienos pridejimas <a class="heading-elements-toggle"><i class="icon-box"></i></a></h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                @include('admin.includes.alert')
                
                <form method="post" action="{{ route('posts.store') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}

                    <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                        <label>Pavadinimas</label>
                        <input class="form-control" name="title" placeholder="Pavadinimas">
                        <p class="help-block">{{ $errors->has('title') ? $errors->first('title') : '' }}</p>
                    </div>

                    <div class="form-group {{ ($errors->has('body') ? 'has-error' : '') }}">
                        <label>Puslapio tesktas</label>
                        <textarea id="editor1" class="form-control input-sm" name="body" placeholder="Naujienos tekstas" rows="10"></textarea>
                        <p class="help-block">{{ $errors->first('body', ':message') }}</p>
                    </div>
                    
                    <div class="form-group {{ $errors->has('photo') ? ' has-error' : '' }}">
                        <label>Paveiksliukas</label>
                        <input type="file" name="photo" class="form-control">
                        <p class="help-block">{{ $errors->has('photo') ? $errors->first('photo') : '' }}</p>
                    </div>

                    <button class="btn btn-primary btn-sm">Pridėti</button>
                    <a href="{{ route('posts.index') }}" class="btn btn-default btn-sm">Grįžti atgal</a>
                </form>
            </div>
        </div>
        <!-- /simple panel -->

    </div>


    <!-- /content area -->
    @push('admin-scripts')
        <script src="//cdn.ckeditor.com/4.7.1/full/ckeditor.js"></script>
        <script>
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace( 'editor1' );
        </script>
    @endpush
@endsection