<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Administration panel</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- /global stylesheets -->
    <link href="{{ asset('administration/css/admin.css') }}" rel="stylesheet" type="text/css">


    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('administration/js/all.js') }}"></script>
    
    <!-- /theme JS files -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
    <body>
        @include('admin.includes.topbar')
        <div class="page-container">
            <div class="page-content">
                @include('admin.includes.sidebar')
                <div class="content-wrapper">
                    @yield('content')
                </div>
            </div>
        </div>
        @stack('admin-scripts')
    </body>
</html>
