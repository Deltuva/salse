@extends('admin.layouts.master')
@section('content')
    @include('admin.includes.header',[
        'title' => 'Skydelis'
    ])
    <!-- Content area -->
    <div class="content">
        <!-- Simple panel -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Statistics<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">

            </div>
        </div>
        <!-- /simple panel -->

    </div>
    <!-- /content area -->

@endsection