@extends('admin.layouts.master')
@section('content')
    @include('admin.includes.header',[
        'title' => 'Redaguoti kategorija',
        'description' => 'Puslapio kategorija'
    ])
    <div class="content">
        <div class="panel panel-flat">
            <div class="panel-body">

                @include('admin.includes.alert')

                <form action="{{ route('categories.update', $category->id) }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="form-group {{ ($errors->has('name') ? 'has-error' : '') }}">
                        <label>Pavadinimas</label>
                        <input class="form-control input-sm" placeholder="Pavadinimas" name="name" value="{{ $category->name }}">
                        <p class="help-block">{{ $errors->first('name', ':message') }}</p>
                    </div>
                    
                    <div class="text-center">
                        <img src="{{ asset('uploads/categories/icons/' . ) }}">
                    </div>

                    <div class="form-group {{ $errors->has('icon') ? ' has-error' : '' }}">
                        <label>Paveiksliukas</label>
                        <input type="file" name="icon" class="form-control">
                        <p class="help-block">{{ $errors->has('icon') ? $errors->first('icon') : '' }}</p>
                    </div>

                    <button type="submit" class="btn btn-primary btn-sm">Redaugoti</button>
                    <a class="btn btn-default btn-sm" href="{{ route('categories.index') }}">Grįžti</a>
                </form>
            </div>
        </div>
    </div>
@endsection