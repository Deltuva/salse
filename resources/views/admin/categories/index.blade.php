@extends('admin.layouts.master')
@section('content')
    @include('admin.includes.header',[
        'title' => 'Kategorijos',
        'description' => 'Svetainės kategoriju koregavimas.',
        'actions' => [
            [
                'title' => 'Kurti kategorija',
                'route' => route('categories.create')
            ]
        ]
    ])
    <!-- Content area -->
    <div class="content">
        <!-- Simple panel -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Kategorijos<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            @if(!$categories->isEmpty())
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <td></td>
                            <td>#</td>
                            <td>Pavadinimas</td>
                            <td></td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody class="sortable" data-entityname="categories">
                        @foreach($categories as $category)
                            <tr class="small" data-itemId="{{ $category->id }}">
                                <td class="sortable-handle"><span class="icon-sort"></span></td>
                                <td>{{ $category->id }}</td>
                                <td>{{ $category->name }}</td>
                                <td><a href="{{ route('categories.edit', $category->id) }}" class="btn btn-primary">Redaguoti</a></td>
                                <td>
                                    <form action="{{ route('categories.destroy', $category->id) }}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button onclick="return confirm('Ar tikrai norite ištrinti kategorija?');" type="submit" class="btn btn-danger btn-sm">Pašalinti</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <div class="panel-body">
                    <div class="alert alert-danger">Kategoriju nera.</div>
                </div>
            @endif
        </div>
        <!-- /simple panel -->

    </div>
    <!-- /content area -->

@endsection