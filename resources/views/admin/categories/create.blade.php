@extends('admin.layouts.master')
@section('content')
    @include('admin.includes.header',[
        'title' => 'Kurti kategorija'
    ])
    <!-- Content area -->
    <div class="content">
        <!-- Simple panel -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Kurti kateogrija<a class="heading-elements-toggle"><i class="icon-box"></i></a></h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                @include('admin.includes.alert')
                <form method="post" action="{{ route('categories.store') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}

                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        <label>Pavadinimas</label>
                        <input class="form-control" name="name" placeholder="Pavadinimas">
                        <p class="help-block">{{ $errors->has('name') ? $errors->first('name') : '' }}</p>
                    </div>

                    <div class="form-group {{ $errors->has('icon') ? ' has-error' : '' }}">
                        <label>Paveiksliukas</label>
                        <input type="file" name="icon" class="form-control">
                        <p class="help-block">{{ $errors->has('icon') ? $errors->first('icon') : '' }}</p>
                    </div>

                    <button class="btn btn-primary btn-sm">Kurti</button>
                    <a href="{{ route('categories.index') }}" class="btn btn-default btn-sm">Grįžti atgal</a>
                </form>
            </div>
        </div>
        <!-- /simple panel -->

    </div>
    <!-- /content area -->
@endsection