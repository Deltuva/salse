@extends('admin.layouts.master')
@section('content')
    @include('admin.includes.header',[
        'title' => 'Naujienos redagavimas',
        'description' => 'Puslapio redagavimas'
    ])
    <div class="content">
        <div class="panel panel-flat">
            <div class="panel-body">

                @include('admin.includes.alert')

                <form action="{{ route('pages.update', $page->id) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="form-group {{ ($errors->has('name') ? 'has-error' : '') }}">
                        <label>Pavadinimas</label>
                        <input class="form-control input-sm" placeholder="Pavadinimas" name="name" value="{{ $page->name }}">
                        <p class="help-block">{{ $errors->first('name', ':message') }}</p>
                    </div>

                    <div class="form-group {{ ($errors->has('content') ? 'has-error' : '') }}">
                        <label>Tekstas</label>
                        <textarea id="editor1" class="form-control input-sm" name="content" placeholder="Puslapio tekstas" rows="10">{{ $page->content }}</textarea>
                        <p class="help-block">{{ $errors->first('content', ':message') }}</p>
                    </div>

                    <button type="submit" class="btn btn-primary btn-sm">Redaugoti</button>
                    <a class="btn btn-default btn-sm" href="{{ route('posts.index') }}">Grįžti</a>
                </form>
            </div>
        </div>
    </div>
    @push('admin-scripts')
    <script src="//cdn.ckeditor.com/4.7.1/full/ckeditor.js"></script>
    <script>
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace( 'editor1' );
    </script>
    @endpush
@endsection