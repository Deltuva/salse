@extends('admin.layouts.master')
@section('content')
    @include('admin.includes.header',[
        'title' => 'Create page'
    ])
    <!-- Content area -->
    <div class="content">
        <!-- Simple panel -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Kurti puslapį<a class="heading-elements-toggle"><i class="icon-box"></i></a></h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                @include('admin.includes.alert')
                <form method="post" action="{{ route('pages.store') }}">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}

                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        <label>Pavadinimas</label>
                        <input class="form-control" name="name" placeholder="Pavadinimas">
                        <p class="help-block">{{ $errors->has('name') ? $errors->first('name') : '' }}</p>
                    </div>

                    <div class="form-group {{ ($errors->has('content') ? 'has-error' : '') }}">
                        <label>Puslapio tesktas</label>
                        <textarea id="editor1" class="form-control input-sm" name="content" placeholder="Puslapio tekstas" rows="10"></textarea>
                        <p class="help-block">{{ $errors->first('content', ':message') }}</p>
                    </div>

                    <button class="btn btn-primary btn-sm">Kurti</button>
                    <a href="{{ route('pages.index') }}" class="btn btn-default btn-sm">Grįžti atgal</a>
                </form>
            </div>
        </div>
        <!-- /simple panel -->

    </div>
    <!-- /content area -->
    @push('admin-scripts')
    <script src="//cdn.ckeditor.com/4.7.1/full/ckeditor.js"></script>
    <script>
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace( 'editor1' );
    </script>
    @endpush
@endsection