@extends('admin.layouts.master')
@section('content')
    @include('admin.includes.header',[
        'title' => 'Puslapiai',
        'description' => 'Svetainės puslapių koregavimas.',
        'actions' => [
            [
                'title' => 'Kurti puslapį',
                'route' => route('pages.create')
            ]
        ]
    ])
    <!-- Content area -->
    <div class="content">
        <!-- Simple panel -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Puslapiai<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            @if(!$pages->isEmpty())
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Pavadinimas</td>
                                <td>Trumpas aprašymas</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody class="sortable" data-entityname="pages">
                            @foreach($pages as $page)
                                <tr class="small" data-itemId="{{ $page->id }}">
                                    <td class="sortable-handle"><span class="icon-sort"></span></td>
                                    <td>{{ $page->id }}</td>
                                    <td>{{ $page->name }}</td>
                                    <td>{{ str_limit(strip_tags($page->content), 200) }}</td>
                                    <td><a href="{{ route('pages.edit', $page->id) }}" class="btn btn-primary">Redaguoti</a></td>
                                    <td>
                                        <form action="{{ route('pages.destroy', $page->id) }}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button onclick="return confirm('Ar tikrai norite ištrinti puslapį?');" type="submit" class="btn btn-danger btn-sm">Pašalinti</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <div class="panel-body">
                    <div class="alert alert-danger">Puslapiu nera.</div>
                </div>
            @endif
        </div>
        <!-- /simple panel -->

    </div>
    <!-- /content area -->

@endsection