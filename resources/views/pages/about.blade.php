@extends('layouts.app') @section('content')

<div class="siteBg {{ $page->xy }}" style="background-image: url({{ $page->bg }});">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @include('includes/header')
            </div>
        </div>
    </div>

    <?php $page_class = $page->content_class; ?>
    <section class="siteContent {{ $page_class }}" style="padding-top: 80px;">
        <div class="container">
            <div class="row no-margin">
                <div class="col-sm-12 col-xs-12">
                    <div class="siteContent__block">
    
                        @if (!is_null($page->name))
                            <div class="siteHeadingTitle">
                                {{ $page->name }}
                            </div>
                        @endif
    
                        @if (!is_null($page->content))
                            <div class="siteParagraphText">
                                {!! $page->content !!}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection