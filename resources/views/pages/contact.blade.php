@extends('layouts.app') @section('content')

<div class="siteBg {{ $page->xy }}" style="background-image: url({{ $page->bg }});">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @include('includes/header')
            </div>
        </div>
    </div>

    <?php $page_class = $page->content_class; ?>
    <section class="siteContent {{ $page_class }}">
        <div class="container">
            <div class="row no-margin">
                <div class="col-md-6 col-md-push-6">
                    <h3>Rekvizitai</h3>

                    @if (!is_null($page->content))
                        <div class="siteParagraphText">
                            {!! $page->content !!}
                        </div>
                    @endif
                </div>
                <div class="col-md-6 col-md-pull-6">
                    <h3 class="text-center">Mus rasite:</h3>

                    <div class="siteContent__map">
                        <div id="map"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@push('scripts')
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_JS_API') }}&callback=initMap"></script>
<script type="text/javascript">
	function initMap() {
			var marker;
			var mapOptions = {
				zoom: 16,
				center: new google.maps.LatLng(56.0600622, 21.4835102),
				mapTypeId: google.maps.MapTypeId.TERRAIN,
			};

            map = new google.maps.Map(document.getElementById('map'), mapOptions);

			var marker = new google.maps.Marker({
				position: {
					lat: 56.0600622,
					lng: 21.4835102,
				},
				map: map
			});
		}
</script>
@endpush

@endsection