@extends('layouts.app') @section('content')

<?php $page = \App\Page::whereContentClass('product')->first(); ?>

<div class="siteBg {{ $page->xy }}" style="background-image: url({{ $page->bg }});">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @include('includes/header')
            </div>
        </div>
    </div>

    <?php $page_class = 'product'; ?>
    <section class="siteContent {{ $page_class }}">
        <div class="container">
            <div class="row no-margin">
                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                    <div class="siteContent__categories">
                        @include('includes/categories')
                    </div>
                </div>

                @include('products/view')
            </div>
        </div>
    </section>
</div>

@endsection