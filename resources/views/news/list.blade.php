<section class="siteContent {{ $page->content_class }}" style="padding-top: 80px;">
        <div class="container">
            <div class="row no-margin">
                <div class="col-sm-12 scrollNews">
                    @if (count($posts) > 0)
                        @foreach ($posts as $post)
                            @if (!is_null($post->title))
                                <div class="siteContent__block">
                                    <div class="row no-margin">
                                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                            @if (!is_null($post->photo))
                                                <img class="thumb img-responsive" src="{{ $post->photo }}" alt="Kraunasi">
                                            @endif
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-7 col-lg-7">
                                            <h4>{{ $post->title }}</h4>
                                            <p>{{ $post->body }}</p>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                            <a href="{!! url('/naujiena', [$post->slug]); !!}" class="btn center-block">Plačiau</a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @else
                        <span class="empty">
                            Nėra jokių naujienų.
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </section>