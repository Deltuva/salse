@extends('layouts.app') @section('content')

<?php $page = \App\Page::whereContentClass('new')->first(); ?>

<div class="siteBg {{ $page->xy }}" style="background-image: url({{ $page->bg }});">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @include('includes/header')
            </div>
        </div>
    </div>

    <?php $page_class = 'newView'; ?>
    <section class="siteContent {{ $page_class }}" style="padding-top: 80px;">
        <div class="container">
            <div class="row no-margin">
                <div class="col-sm-12 col-xs-12 no-padding-right">
                    <div class="siteContent__block">
                        @if (!is_null($post->photo))
                            <img class="center-block m-b-15 img-responsive" src="{{ $post->photo }}" alt="Kraunasi">
                        @endif

                        @if (!is_null($post->title))
                        <div class="siteHeadingTitle">
                            {{ $post->title }}
                        </div>
                        @endif @if (!is_null($post->body))
                        <div class="siteParagraphText">
                            {!! $post->body !!}
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection