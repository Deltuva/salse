<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'title' => $faker->text(35),
        'body' => $faker->text(255),
        'slug' => str_slug($faker->title),
        'photo' => 'http://lorempixel.com/400/400/',
    ];
});
