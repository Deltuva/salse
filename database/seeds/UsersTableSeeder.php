<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        User::create([
            'name' => 'slowas',
            'email' => 'exception321@gmail.com',
            'password' => bcrypt('password'),
            'permission' => 'Administrator'
        ]);

        User::create([
            'name' => 'minde',
            'email' => 'deltuva.mindaugas@gmail.com',
            'password' => bcrypt('password'),
            'permission' => 'Administrator'
        ]);

    }
}
