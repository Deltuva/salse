<?php

use Illuminate\Database\Seeder;

class PageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        $page = \App\Page::create([
            'name' => 'Apie mus',
            'content' => $faker->text(),
            'content_class' => 'about',
            'slug' => 'apie-mus',
            'bg' => '/images/backgrounds/about.jpg',
            'xy' => '',
        ]);

        $page = \App\Page::create([
            'name' => 'Produktai',
            'content' => $faker->text(),
            'content_class' => 'product',
            'slug' => 'produktai',
            'bg' => '/images/backgrounds/product.jpg',
            'xy' => ''
        ]);

        $page = \App\Page::create([
            'name' => 'Naujienos',
            'content' => $faker->text(),
            'content_class' => 'new',
            'slug' => 'naujienos',
            'bg' => '/images/backgrounds/new.jpg',
            'xy' => 'bottom'
        ]);

        $page = \App\Page::create([
            'name' => 'Kontaktai',
            'content' => '<div class="siteContent__block">
            <div class="siteHeadingTitle"><span style="font-size:22px"><strong>Salsė, UAB</strong></span></div>
            
            <div class="siteParagraphText">
            <div>
            <p>Įmonės kodas: 303247400</p>
            
            <p>PVM mokėtojo kodas: LT100008393510</p>
            
            <p>Adresas Salantų g. 13, Bajoraliai, LT-97311 Kretingos r.</p>
            </div>
            
            <div>
            <p>Vadovas: Romualdas Mickus</p>
            
            <p>Tel. +370 686 06935</p>
            
            <p>El. pa&scaron;tas: info@salse.lt</p>
            </div>
            </div>
            </div>',
            'content_class' => 'contact',
            'slug' => 'kontaktai',
            'bg' => '/images/backgrounds/contact.jpg',
            'xy' => ''
        ]);

        if ($page)
            echo "Page generated... \n";
    }
}
