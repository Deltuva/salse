<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $category = \App\Category::create([
            'name' => 'Kategorija1',
            'icon' => 'icon-layer9'
        ]);

        $category = \App\Category::create([
            'name' => 'Kategorija2',
            'icon' => 'icon-layer11'
        ]);

        $category = \App\Category::create([
            'name' => 'Kategorija3',
            'icon' => 'icon-layer8'
        ]);

        $category = \App\Category::create([
            'name' => 'Kategorija4',
            'icon' => 'icon-layer12'
        ]);

        $category = \App\Category::create([
            'name' => 'Kategorija5',
            'icon' => 'icon-layer10'
        ]);

        if ($category)
            echo "Category generated... \n";
    }
}
