let mix = require('laravel-mix');
let bourbon = require('bourbon').includePaths;
let neat = require('bourbon-neat').includePaths;

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js([
	'resources/assets/js/app.js'
], 'public/js/all.js')
.sass('resources/assets/sass/app.sass', 'public/css', null, {
	includePaths: bourbon.concat(neat)
});

mix.styles([
	'resources/assets/administration/css/bootstrap.css',
	'resources/assets/administration/css/core.css',
	'resources/assets/administration/css/components.css',
	'resources/assets/administration/css/colors.css',
	'resources/assets/administration/css/icons/icomoon/styles.css'
], 'public/administration/css/admin.css');

mix.scripts([
	'resources/assets/administration/js/core/libraries/jquery.min.js',
	'resources/assets/administration/js/core/libraries/bootstrap.min.js',
	'resources/assets/administration/js/core/libraries/jquery_ui/interactions.min.js',
	'resources/assets/administration/js/core/libraries/jquery_ui/touch.min.js',
	'resources/assets/administration/js/core/app.js',
	'resources/assets/administration/js/main.js',
], 'public/administration/js/all.js');

mix.disableNotifications();

if (mix.inProduction()) {
    mix.version();
}