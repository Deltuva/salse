<?php
Route::group(['middleware' => 'web'], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/puslapis/{page}', 'PageController@pageShow')->name('pageShow');
    Route::get('/naujienos-puslapis/{slug}', 'PostController@postShow')->name('postShow');
    Route::post('sort', '\Rutorika\Sortable\SortableController@sort')->name('sort');
    Route::get('logout', 'HomeController@logout')->name('logout');

    //ajax
    Route::get('/ajax/product/{productId}', 'Api\ApiController@getProduct')->name('ajaxProduct');
});

Route::group(['middleware' => 'admin', 'prefix' => 'admin'], function() 
{
    Route::get('/', 'Admin\AdminController@index')->name('admin.home');

    Route::resource('users', 'Admin\Users\UsersController');
    Route::resource('categories', 'Admin\Categories\CategoriesController');
    Route::resource('pages', 'Admin\Pages\PagesController');
    Route::resource('products', 'Admin\Products\ProductsController');
    Route::resource('posts', 'Admin\Posts\PostsController');
    Route::resource('settings', 'Admin\Settings\SettingsController');

});

Auth::routes();